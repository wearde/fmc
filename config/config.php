<?php

use Fms\core\Job;
use Fms\core\JobInterface;
use Fms\core\Logger;
use Fms\core\LoggerInterface;
use Fms\core\Queue;
use Fms\storage\StorageInterface;
use Psr\Container\ContainerInterface;

/**
 * DI configuration
 */
return [
  'dependencies' => [
    'abstract_factories' => [
      Zend\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory::class,
    ],
    'aliases' => [
      JobInterface::class => Job::class,
      LoggerInterface::class => Logger::class,
    ],
    'factories' => [
      SQLite3::class => function (ContainerInterface $container) {
        return new SQLite3($container->get('config')['db_path']);
      },
      StorageInterface::class => function (ContainerInterface $container) {
        return new \Fms\storage\DbStorage(
          $container->get(SQLite3::class)
        );
      },
      Queue::class => function (ContainerInterface $container) {
        return new Queue(
          $container->get(JobInterface::class),
          $container->get(LoggerInterface::class),
          $container->get('config')['sleepTime']
        );
      }
    ],
  ],
  'db_path' => 'data/db.sqlite',
  /**
   * 0.1 second for tests
   */
  'sleepTime' => 100000
];
