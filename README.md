PHP FMC Challenge
================================

Installation
------

Clone the repository for `pull` command availability:

~~~
git clone git@bitbucket.org:wearde/fmc.git project
cd project
composer install
~~~

Build the test suite:

```
vendor/bin/phpunit
```
or

```
composer test
```


