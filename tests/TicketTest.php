<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

use Fms\core\Queue;
use Fms\storage\StorageInterface;
use Fms\Ticket;
use PHPUnit\Framework\TestCase;

class TicketTest extends TestCase
{

  /**
   * @var Ticket
   */
  private $ticket;
  /**
   * @var \Psr\Container\ContainerInterface $container
   */
  private $container;

  public function setUp()
  {
    parent::setUp();

    $this->container = require 'config/container.php';
    try {
      $this->ticket = new Ticket($this->container->get(StorageInterface::class), $this->container->get(Queue::class));
    } catch (\Psr\Container\NotFoundExceptionInterface $e) {
    } catch (\Psr\Container\ContainerExceptionInterface $e) {
    }
  }

  /**
   * test data
   */
  public function testData()
  {
    $received = 0;
    $rejected = 0;
    $severed = 0;
    $i = 1;
    while ($i <= 1000) {
      try {
        $h = random_int(1, 5);
      } catch (Exception $e) {
      }
      for ($j = 1; $j <= $h; $j++) {

        try {
          $this->ticket->get(rand(1, 999));
          $received += 1;
        } catch (\Fms\exceptions\QueueFullException $e) {
          $severed += 1;

        } catch (\Fms\exceptions\TicketExistException $e) {
          $rejected += 1;
        }
      }
      $this->ticket->push();
      try {
        usleep($this->container->get('config')['sleepTime']);
      } catch (\Psr\Container\NotFoundExceptionInterface $e) {
      } catch (\Psr\Container\ContainerExceptionInterface $e) {
      }
      $i += $h;
    }
    $this->ticket->save();
    $this->assertTrue(true);
    echo PHP_EOL;
    echo 'Number of request received ' . $received. PHP_EOL;
    echo 'Number of requested rejected ' . $rejected. PHP_EOL;
    echo 'Number of requests severed ' . $severed. PHP_EOL;
  }
}
