<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * @var yii\web\View $this
 */

namespace Tests;

use Fms\core\Job;
use Fms\core\LoggerInterface;
use Fms\core\Queue;
use PHPUnit\Framework\TestCase;

class QueueTest extends TestCase
{
  /**
   * @var Queue
   */
  private $queue;

  public function setUp()
  {
    parent::setUp();
    $this->queue = new Queue(new Job(), new DummyLogger());
  }

  public function testSetTimeOut()
  {
    $queue = $this->queue->setTimeOut(2);
    $this->assertEquals(2, $this->queue->getTimeOut());
    $this->assertEquals($queue, $this->queue);
  }

  /**
   * @throws \Fms\exceptions\QueueFullException
   */
  public function testAddLessThanTen()
  {
    $this->queue->add('1234');
    $this->queue->add('12344');
    $this->queue->add('12345');
    $this->assertEquals(3, count($this->queue->getQueue()));
  }

  /** testAddMoreThanTen
   * @dataProvider   arrayData
   * @expectedException \Fms\exceptions\QueueFullException;
   * @param $array
   */
  public function testAddMoreThanTen($array)
  {
    try {
      array_map(function ($el) {
        $this->queue->add($el);
      }, $array);
    } catch (\Exception $exception) {
    }
    $this->assertEquals(10, count($this->queue->getQueue()));
  }


  /** testAddMoreThanTen
   * @dataProvider   arrayData
   * @expectedException \Fms\exceptions\QueueFullException;
   * @param $array
   */
  public function testSaved($array)
  {
    try {
      array_map(function ($el) {
        $this->queue->add($el);
      }, $array);
    } catch (\Exception $exception) {
    }
    $this->queue->push();
    $this->assertEquals(2, count($this->queue->getSaved()));
  }


  public function arrayData()
  {
    return [
      [['rr', 'dsd', 'dfdf', 'dfdfd', 'dfdfd', 'rr', 'dsd', 'dfdf', 'dfdfd', 'dfdfd', 'sd']]
    ];
  }
}

class DummyLogger implements LoggerInterface
{
  public function write(string $log): void
  {
  }
}
