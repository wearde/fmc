<?php
/**
 * This file is part of the Sursil shop
 *
 * @copyright 2018 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 *
 * @var yii\web\View $this
 */

namespace Tests;

use Fms\core\Job;
use PHPUnit\Framework\TestCase;

class JobTest extends TestCase
{
  /**
   * @var Job
   */
  private $job;

  public function setUp()
  {
    parent::setUp();
    $this->job = new Job();
  }

  public function testEmpty()
  {
    list($queue, $result) = $this->job->do([]);
    $this->assertEquals([], $queue);
    $this->assertEquals([], $result);
  }

  public function testJustOne()
  {
    list($queue, $result) = $this->job->do([55]);
    $this->assertEquals([], $queue);
    $this->assertEquals([55], $result);
  }

  public function testMoreThanOne()
  {
    list($queue, $result) = $this->job->do([55, 66, 54]);
    $this->assertEquals([54], $queue);
    $this->assertEquals([55, 66], $result);
  }
}
