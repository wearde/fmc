<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\core;

/**
 * Class Logger
 * @package Fms\core
 */
class Logger implements LoggerInterface
{
  /**
   * Write logs
   * @param string $log
   */
  public function write(string $log): void
  {
    file_put_contents('data/queue_size.log', $log, FILE_APPEND);
  }
}