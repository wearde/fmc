<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\core;

/**
 * Class Job
 * @package Fms\core
 */
class Job implements JobInterface
{
  /**
   * @param array $queue
   * @return array
   */
  public function do(array $queue): array
  {
    $result = [];
    $i = 1;
    while ($i <= 2) {
      if ($queue) {
        $result[] = array_shift($queue);
      }
      $i++;
    }
    return [$queue, $result];
  }
}