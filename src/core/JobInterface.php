<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\core;

interface JobInterface
{
  public function do(array $queue): array;
}