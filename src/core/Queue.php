<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\core;

use Fms\exceptions\QueueFullException;

/**
 * Class Queue
 * @package Fms\core
 */
class Queue
{
  /**
   * @var array
   */
  private $queue = [];
  /**
   * @var array
   */
  private $saved = [];
  /**
   * microseconds 1c === 1000000
   * @var int
   */
  private $sleep;
  /**
   * @var Logger
   */
  private $logger;
  /**
   * @var Job
   */
  private $job;

  /**
   * Queue constructor.
   * @param JobInterface $job
   * @param LoggerInterface $logger
   * @param int $sleep
   */
  public function __construct(JobInterface $job, LoggerInterface $logger, $sleep = 1)
  {
    $this->logger = $logger;
    $this->job = $job;
    $this->sleep = $sleep;
  }

  /**
   * @param int $time
   * @return Queue
   */
  public function setTimeOut(int $time): self
  {
    $this->sleep = $time;
    return $this;
  }

  /**
   * @return int
   */
  public function getTimeOut(): int
  {
    return $this->sleep;
  }

  /**
   * Add data to queue
   * @param $data
   * @return Queue|null
   * @throws QueueFullException
   */
  public function add(string $data): self
  {
    if (count($this->queue) < 10) {
      array_push($this->queue, $data);
    } else {
      throw new QueueFullException('Queue is full');
    }
    return $this;
  }

  /**
   * Push the Queue to doing method & write count of queue
   */
  public function push(): void
  {
    if ($this->queue) {
      $this->logger->write(count($this->queue) . PHP_EOL);
      list($queue, $result) = $this->job->do($this->queue);
      $this->queue = $queue;
      $this->saved = array_merge($result, $this->saved);
      usleep($this->sleep);
    }
  }

  /**
   * @return array
   */
  public function clear(): array
  {
    while ($this->queue) {
      $this->push();
    }
    return $this->saved;
  }

  /**
   * @return array
   */
  public function getQueue(): array
  {
    return $this->queue;
  }

  /**
   * @return array
   */
  public function getSaved(): array
  {
    return $this->saved;
  }
}