<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\core;

interface LoggerInterface
{
  public function write(string $log): void;
}