<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms;

use Fms\core\Job;
use Fms\core\Queue;
use Fms\exceptions\TicketExistException;
use Fms\storage\StorageInterface;

/**
 * Class Ticket
 * @package Fms
 */
class Ticket
{
  /**
   * @var StorageInterface
   */
  private $storage;
  /**
   * @var Queue
   */
  private $queue;

  /**
   * Ticket constructor.
   * @param StorageInterface $storage
   * @param Queue $queue
   */
  public function __construct(StorageInterface $storage, Queue $queue)
  {
    $this->storage = $storage;
    $this->queue = $queue;
  }

  /**
   * @return array
   */
  public function all(): array
  {
    return $this->storage->load();
  }

  /**
   * @param string $uid
   * @throws TicketExistException
   * @throws exceptions\QueueFullException
   */
  public function get(string $uid): void
  {
    if (in_array($uid, $this->storage->load())) {
      throw new TicketExistException('Ticket ' . $uid .' is exist in Queue');
    }
    $this->queue->add($uid);
  }

  public function push(): void
  {
    $this->queue->push();
  }

  public function save(): void
  {
    $result = $this->queue->clear();
    $this->storage->save($result);
  }
}