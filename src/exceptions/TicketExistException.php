<?php
/**
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\exceptions;

class TicketExistException extends \Exception
{

}