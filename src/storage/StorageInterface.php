<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\storage;

interface StorageInterface
{
  public function load(): array;

  /**
   * @param array $value
   */
  public function save(array $value): void;
}