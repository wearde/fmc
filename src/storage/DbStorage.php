<?php
/**
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Fms\storage;

use SQLite3;

class DbStorage implements StorageInterface
{
  /**
   * @var SQLite3
   */
  private $db;

  private $cached = [];

  public function __construct(SQLite3 $connection)
  {
    $this->db = $connection;
    $this->createTable();
  }

  public function load(): array
  {
    if (!$this->cached) {
      $query = $this->db->query("SELECT ticket_uniq FROM tickets");

      while ($data = $query->fetchArray(SQLITE3_ASSOC)) {
        if (!empty($data['ticket_uniq'])) {
          array_push($this->cached, $data['ticket_uniq']);
        }
      }
    }

    return $this->cached;
  }

  public function save(array $data): void
  {
    $insert = "INSERT INTO tickets (ticket_uniq) VALUES (:ticket_uniq)";
    $prepare = $this->db->prepare($insert);
    foreach ($data as $value) {
      if ($value) {
        $prepare->bindParam(':ticket_uniq', $value);
        $prepare->execute();
      }
    }
  }

  private function createTable(): void
  {
    $command = "CREATE TABLE IF NOT EXISTS tickets (
      ticket_id   INTEGER PRIMARY KEY,
      ticket_uniq VARCHAR (255) NOT NULL,
      created_date INTEGER
    )";
    $this->db->exec($command);
  }
}